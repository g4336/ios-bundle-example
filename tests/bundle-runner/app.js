const { describe, it, after, beforeEach} = require('node:test');
const assert = require('node:assert');

const http = require('http');
const finalhandler = require('finalhandler');
const serveStatic = require('serve-static');

const puppeteer = require('puppeteer');
// import {KnownDevices} from 'puppeteer';

const fs = require('fs');

const NodeStreamZip = require('node-stream-zip');

const unpackDir = 'unpacked/';

// Not all games fire the same events
const validGameStates = { "GameState.GAME_IDLE" : false, "GameState.PRELOAD_COMPLETE" : false, "GameState.GAME_READY" : false, "GameState.LOAD_COMPLETE" : false };

let config = {};

let currentDir = process.cwd();
if(currentDir.indexOf("bundle-runner") == -1)
{
	//we are not in the right dir. happens when you debug
	process.chdir(currentDir + "/tests/bundle-runner");
}

if (fs.existsSync('./config.json') ) {
    let configData = fs.readFileSync('./config.json');
	config = JSON.parse(configData);
}

// Read command-line overrides
var cliVars = {};

process.argv.forEach(function (val, index, array) {
	if (val.startsWith('--')) {
		var tuple = val.substring(2).split("=",2);
		if (Array.isArray(tuple) && tuple.length == 2) {
			cliVars[tuple[0]] = tuple[1];
		}
	}
});

// Extract params from config or override with cli args
var defaultGameServer = 'rgs-eu-sta';
if (config.defaultGameServer) {
	defaultGameServer = config.defaultGameServer;
}
if (cliVars["defaultGameServer"] !== undefined) {
	defaultGameServer = cliVars["defaultGameServer"]
}
var headless = true;
if (config.headless !== undefined) {
	headless = config.headless
}
if (cliVars["headless"] !== undefined) {
	headless = cliVars["headless"]
}
var timeout = 5000;
if (!headless) { timeout = 10000; }
if (config.timeout !== undefined) {
	timeout = config.timeout
}
if (cliVars["timeout"] !== undefined) {
	timeout = cliVars["timeout"]
}
let bundlesDir = '../../iOSBundleExample/iOSBundleExample/game-bundles/';
if (config.bundlesDir !== undefined) {
	bundlesDir = config.bundlesDir
}
if (cliVars["bundlesDir"] !== undefined) {
	bundlesDir = cliVars["bundlesDir"]
}
var port = 8002;
if (config.port) {
	port = config.port;
}
if (cliVars["port"] !== undefined) {
	port = cliVars["port"]
}
var limitedGameList = [];
if (config.limitedGameList) {
	limitedGameList = config.limitedGameList;
}
if (cliVars["limitedGameList"] !== undefined) {
	var cliGameArray = cliVars["limitedGameList"].split(",");
	if (Array.isArray(cliGameArray)) {
		limitedGameList = cliGameArray;
	}
}

// Check params
if (!fs.existsSync(bundlesDir)) {
    throw('Unable to find bundles directory: "'+bundlesDir+'"')
}
if (!fs.existsSync(bundlesDir+'gmr-game-config.json')) {
    throw('Unable to find bundles JSON file: "'+bundlesDir+'gmr-game-config.json"')
}

let bundleData = fs.readFileSync(bundlesDir+'gmr-game-config.json');
let bundles = JSON.parse(bundleData);

var serve = serveStatic("./");

var server = http.createServer(function(req, res) {
  	var done = finalhandler(req, res);
	serve(req, res, done);
});

server.listen(port);

const delay = (milliseconds) => new Promise((resolve) => setTimeout(resolve, milliseconds));
var count = bundles.length;
if (Array.isArray(limitedGameList) && limitedGameList.length > 0) {
	count = limitedGameList.length
}
var counter = 0;

bundles.forEach(function (bundle, index) {
	if (!limitedGameList || limitedGameList.length == 0 || limitedGameList.includes(bundle.gameId)) {

		var bail = false;
		var gameServer = defaultGameServer;
		if (config.gameServerOverride && config.gameServerOverride[bundle.gameId]) {
			gameServer = config.gameServerOverride[bundle.gameId];
		}
	
		describe('['+counter+'/'+count+'] '+bundle.gameId+' Bundle Launch Tests', async () => {
		
			after(() => {
				counter++;
				fs.rmSync(unpackDir+bundle.gameId, { recursive: true, force: true });
				if (counter === count || (Array.isArray(config.limitedGameList) && config.limitedGameList.length > 0 && counter === config.limitedGameList.length)) {
					server.close();
					fs.rmSync(unpackDir+'*', { recursive: true, force: true });
				}
			});
		
			await it(bundle.gameId+ " Unzip", async (t) => {
				if (bail) {	return t.skip(); }
				try {
				  await unzipFile(bundlesDir+bundle.archive,  unpackDir+bundle.gameId );
				} catch (err) { 
					bail = true;
					assert.fail("Archive Extraction failed: "+err)
				}
			});
	
			await it(bundle.gameId+ " Contents", async (t) => {
				if (bail) {	return t.skip();}
				if (!fs.existsSync(unpackDir+bundle.gameId+'/'+bundle.launchPath)) {
					bail = true;
					assert.fail("Launch path unavailable")
				}
			});
	
			await it(bundle.gameId+ " Launch", async (t) => 
			{
				if (bail) {	return t.skip(); }
				const iPhone = puppeteer.KnownDevices['iPhone 12'];
				const browser = await puppeteer.launch({headless: headless});
				const page = await browser.newPage();
				await page.emulate(iPhone);

				let lang = 'en-GB';
				let operator = 'gr-test';
				let accountId = 'pete';
				let currency = 'GBP';
				let device = 'phone';
				let forceDevice = 'phone';
				let gameMode = 'CASH';
				let token = '1708684019770';
				let pcurl = 'https%3A%2F%2Falchemy-sta.gamingrealms.net%2Frgs%2Fwrapper%2Fso-wrapper.min.js';
				const url = 'http://localhost:'+port+'/'+unpackDir+bundle.gameId+'/index.html?gameServer='+gameServer+
																						'&gameId='+bundle.gameId+
																						'&accountId='+accountId+
																						'&locale='+lang+
																						'&currency='+currency+
																						'&operator='+operator+
																						'&token='+token+
																						'&device='+device+
																						'&gameMode='+ gameMode+
																						//'&pcurl=' + pcurl + //4tp games do not load without this
																						'&forceDevice='+forceDevice;
				await page.goto(url, {
					waitUntil: 'networkidle0'
				});
				const foundAll = await pollGameState(page);
				await browser.close();
				if (!foundAll) { 
					bail = true;
					assert.fail("Game Launch error: ");
				}
			}); 
		})
	}
});

//process.exit(0);

const pollGameState = async (page, startTime = Date.now()) => {
	try {
		const gameState = await page.evaluate(function() { 
			try {
				return so.Wrapper.getInstance().model.gameState;
			} catch (err) {
				return err;
			}
		});
		
		if(gameState != null) {				
			let id = null;
			if (typeof gameState === 'object' || gameState instanceof Object) {
				id = gameState.id || gameState.getId();
			}
			if (typeof gameState === 'string' || gameState instanceof String) {
				id = gameState;
			}

			if (validGameStates[id] != undefined && validGameStates[id] != true) {
				return true;
			} else {
				console.log("Game state:", id);
			}
		}	
		
		if (Date.now() - startTime < timeout) {
			// Continue polling until timeout
			await delay(16); // Polling at approximately 60 frames per second
			return pollGameState(page, startTime);
		} else {
			console.log("Timeout occurred while waiting for game state.");
			return null; // Return null if timeout occurs
		}
	} catch(e) {
		console.error(e)
	}		
} 
	
// Async function to unzip a file
const unzipFile = async (zipFilePath, extractToPath) => {
  return new Promise((resolve, reject) => {
    const zip = new NodeStreamZip({
      file: zipFilePath,
      storeEntries: true
    });

    zip.on('ready', () => {
      zip.extract(null, extractToPath, (err, count) => {
        zip.close();
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      });
    });

    zip.on('error', (err) => reject(err));
  });
};
