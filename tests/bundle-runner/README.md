# Bundle Runner

This test runner is intended to allow you to test game-bundles outside of the iOS app.
It runs off the gmr-game-config.json file in the bundles directory and checks :
+ Unzip of file
+ Does the index.html exist
+ Does it load using a built-in webserver and get to a status of "GameState.GAME_IDLE" or "GameState.PRELOAD_COMPLETE"


## Configuration
By default the runner will iterate through every zip archive in the bundles directory and test each one as above. You can configure the runner using the following params.

### Example
```json
{
	"headless" : false,
	"timeout" : 12000,
	"bundlesDir" : "../../iOSBundleExample/StagingiOSBundleExample/game-bundles-staging/",
	"limitedGameList" : ["slingo-redwing"],
	"defaultGameServer" : "rgs-eu-sta",
	"gameServerOverride" : {
		"slingo-redwing" : "rgs-us-mi-sta"
	}
}
```

### port
The port used to run the webserver on.
Defaults to ```8002```

### headless
Boolean, whether the Chrome browser should run as a background process or actually fire up the application on your desktop.
Defaults to true

### timeout
How long (in milliseconds) to allow the browser to load the game before checking for GameStatus.
Defaults to 12000 for headless=false and 8000 for headless=true

### bundlesDir
Alternate location for a bundles directory.

### limitedGameList
A list of games (by gameId) to test, rather than running through all games.

### defaultGameServer
The RGS Game Server to launch the games against (see below).

### gameServerOverride
Specific games to be launched against specific servers, some games (particularly reskins) are not deployed to all servers and so are only available on specific RGS game servers.

Server keys are available in the [Gaming Realms Client Partner Integration Guide](https://docs.google.com/document/d/1Qn4uEO8hjCP_DjfCgyP3XjJ4DEyrhIbYAtZIjiCX2TY/edit#heading=h.oced8v4r03ez)

## Command Line Options
All configuration parameters, excluding ```gameServerOverride``` can be overridden using command line options, for example ```--limitedGameList=slingo-extreme,slingo-xxxtreme``` will allow you to run tests only on these two games.

## Usage

Requires Node v21.0.0 or greater

### Standard usage
```shell
node app.js
```

### Command Line Option Usage
```shell
node ./app.js --limitedGameList=slingo-extreme,slingo-xxxtreme --defaultGameServer=rgs-us-eu-sta
```

The script should be run from inside its parent directory, running from elsewhere is not supported.

