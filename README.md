# iOS Bundle Example

This project is intended as an example of how Gaming Realms iOS Bundles can be incorporated and distributed as part of an Operator's iOS App. It uses Apple's One-demand resources functionality to allow the app bundle install size to be kept to a minimum.
The project is licenced under the OSS MIT licence which means that you are free to copy use and distribute any of the code contained here as part of your own project.

The actual game archives vary depending on the type of integration and/or aggregator used. Please talk to you commercial partner at Gaming Realms to get access to the correct game archive project for your needs.

# Getting Started

First you must clone (or download and archive of) this project to your development machine. Once downloaded, you shoudl be able to open the project in XCode and do a build to confirm that everthing has set up correctly in your environment.

The next step is to link the correct git sub-module that contains the game archives. For the purposes of this guide we will assume that you are using the standard bundles used by most Operators.

## Link Submodule

Open a terminal window and link the GMR Bundle sub-module into your XCode project. This shoudl be done from within the main project folder, not the repository root.
```shell
git submodule add https://gitlab.com/g4336/game-bundles.git
```

## Add Submodule and Archive Files to Project
Once the Submodule has been added to the file-system, you need to add them into your XCode project.
![Add Submodule and Archive Files to Project](docs/add-sub-module.png "Add Submodule and Archive Files to Project")

Locate the new submodule folder in the file-system.
Take care to specify that the added folder is added as "Group" NOT a "Folder"
![Add as Group](docs/add-as-group.png "Add as Group")

Here you can see how the archives should look once added correctly to the project. If the folder is solid blue in colour, you have done this wrong.

![How it should look](docs/imported-archive.png "Imported archives")

If this is the case, remove it, selecting "Remove References" and carefully add again as specfied above.

Set the On Demand Resource Tag for each archive.
![Set tag](docs/set-odr-tag-and-membership.png "Set tag")
It is important that the tag is set the same as the tag in the JSON file in the game-bundle project. The example code currently relies on this setting. In thsi example the tag mirrors teh gameId.
Also make sure that each zip archive is a member of your project in the Target Membership section.

Once this has been done for each of the zip archives you should be able to see them all correctly identified in the Resource Tags section of the Build Target.
Each tag should have only one zip archive in it and should be in the Download Only On Demand section.
![Download Only On Demand](docs/resource-tags.png "Download Only On Demand")

## See Also
+ [Other features of the example app](docs/OTHER_APP_FEATURES.md)
+ [Scripts and Tools](docs/SCRIPTS_AND_TOOLS.md)