#!/bin/bash

node ./tag.js
echo 'Backing up existing project.pbxproj to /tmp/project.pbxproj-prev'
cp ../iOSBundleExample/iOSBundleExample.xcodeproj/project.pbxproj /tmp/project.pbxproj-prev
echo 'Moving new project.pbxproj into place' 
cp ./output.pbxproj ../iOSBundleExample/iOSBundleExample.xcodeproj/project.pbxproj
echo 'Complete'
