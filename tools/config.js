var fs  = require("fs");
var projectFile = '../iOSBundleExample/iOSBundleExample.xcodeproj/project.pbxproj';

var projectFileArray = fs.readFileSync(projectFile).toString().split('\n');
var configFile = '';

const fileRE = /^.*isa = PBXBuildFile; fileRef = .* ((GMR-(.*?))[0-9\.\-]*-ios.*\.zip) \*\/;.*$/

for(var line of projectFileArray) {
    var found = line.match(fileRE);
    if (found) {
        tag = found[2];
        configFile += '    {"gameId": "'+found[3]+'", "tag": "'+found[2]+'", "archive": "'+found[1]+'", "title": "'+found[3].replace("-", " ")+'", "launchPath": "index.html"},'+"\n";
    }
}


fs.writeFile('./output.json', "[\n"+configFile+"]\n", function (err) {
    if (err) throw err;
    console.log('Config file written to ./output.json');
});
