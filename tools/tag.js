var fs  = require("fs");
var projectFile = '../iOSBundleExample/iOSBundleExample.xcodeproj/project.pbxproj';

var projectFileArray = fs.readFileSync(projectFile).toString().split('\n');
var newProjectFile = '';
var knownAssetTags = '';

const fileRE = /(^.*isa = PBXBuildFile; fileRef = .* (GMR-.*?)*-ios.*\.zip \*\/;).*$/
const tagsRE = /KnownAssetTags = \(([\s\S]*?)\);/

for(var line of projectFileArray) {
    var found = line.match(fileRE);
    if (found) {
        tag = found[2];
        line = found[1]+' settings = {ASSET_TAGS = ("'+tag+'", ); }; };'
        knownAssetTags += '"'+tag+'",'+"\n";
    }
    newProjectFile += line+"\n"
}

newProjectFile = newProjectFile.replace(tagsRE, 'KnownAssetTags = ('+knownAssetTags+');');

fs.writeFile('./output.pbxproj', newProjectFile.trim(), function (err) {
    if (err) throw err;
    console.log('New file written to ./output.pbxproj. BACK UP your existing project.pbxproj before replacing.');
});
