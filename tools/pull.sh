echo "\n\033[36mgame-bundles\033[0m update..."
cd ../iOSBundleExample/iOSBundleExample/game-bundles
echo "Pulling `pwd`"
git pull
cd -

echo "\n\n\033[32mgame-bundles-staging\033[0m update..."
cd ../iOSBundleExample/StagingiOSBundleExample/game-bundles-staging
echo "Pulling `pwd`"
git pull
cd -

