//
//  WebServerManager.swift
//  iOSBundleExample
//
//  Created by Peter Russell on 04/03/2024.
//

import Foundation
import OSLog

class WebServerManager: NSObject, GCDWebServerDelegate {
    
    static let shared = WebServerManager()
    private var gcdWebServer: GCDWebServer? = nil
    private var hasInit = false

    public static var isReady: Bool {
        return shared.hasInit;
    }
    
    public static var httpd: GCDWebServer {
        if shared.gcdWebServer == nil {
            shared.startWebServer()
        }
        return shared.gcdWebServer!
    }
    
    public static func start() {
        shared.startWebServer()
    }
    
    func webServerDidStart(_ server: GCDWebServer) {
        os_log("%@", type: .debug, "webServerDidStart PORT:\(server.port)")
        GMRBundleConfiguration.httpdPort = server.port
        self.hasInit = true;
    }
    
    func startWebServer() {
        os_log("%@", type: .debug, "startWebServer")
        GCDWebServer.setLogLevel(4)
        gcdWebServer = GCDWebServer()
        gcdWebServer!.addGETHandler(forBasePath: "/", directoryPath: GMRBundleConfiguration.documentRootURL.path, indexFilename: nil, cacheAge: 3600, allowRangeRequests: true)
        gcdWebServer!.delegate = self
        do {
            try gcdWebServer!.start(options: [ "Port": GMRBundleConfiguration.httpdPort, "BindToLocalhost": true ] )
        } catch {
            // Couldn't create audio web server, log the error
            os_log("%@", type: .debug, "Couldn't start Webserver \(error)")
        }
    }
    
}
