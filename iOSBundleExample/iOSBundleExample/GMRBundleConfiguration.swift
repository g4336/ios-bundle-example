//
//  GMRBundleConfiguration.swift
//  iOSBundleExample
//
//  Created by Pete Russell on 03/11/2021.
//

import Foundation
import OSLog
import UIKit

public struct GMRBundleConfiguration {

    public static var useWebServer: Bool {
        return defaults.string(forKey: "server") ?? "web" == "web"
    }
    public static var forceMobileBrowser: Bool {
        return defaults.string(forKey: "ua") ?? "default" == "force-mobile"
    }
    
    public static var targetName: String {
        let targetName =  Bundle.main.infoDictionary?["CFBundleName"] as! String
        return targetName
    }
    public static var httpdPort : UInt = 0
    public static var documentRootURL : URL = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first ?? URL(fileURLWithPath: "/")
    public static var launchURL : URL {
        if GMRBundleConfiguration.useWebServer {
            return URL(string: "http://localhost:\(httpdPort)/")!
        }
        return  FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first ?? URL(fileURLWithPath: "/")
    }
    public static var gameLookup : [String : Int] = [:]
    static let defaults = UserDefaults.standard
    public static var environment: String {
        return defaults.string(forKey: "environment_preference") ?? "https://rgs-eu-sta.gamingrealms.net"
    }
    public static var preferenceAccountId: String {
        var accountId = defaults.string(forKey: "account_id")
        if accountId != nil && accountId?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            return accountId!.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        accountId = UIDevice.current.identifierForVendor?.uuidString
        if accountId != nil && accountId?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            return accountId!.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        return "uuidunavailable"
    }

    public static var maxGameCacheSizeMB: UInt64 {
        return UInt64(defaults.string(forKey: "cache_size_preference") ?? "200") ?? 200
    }
    public static var locale: String {
        return defaults.string(forKey: "locale") ?? "en-GB"
    }
    public static var currency: String {
        return defaults.string(forKey: "currency") ?? "GBP"
    }
    public static var gameMatadata: [String : GamePlay] {
        if let data = defaults.object(forKey: "gmr_game_metadata") as? Data {
            return try! PropertyListDecoder().decode([String : GamePlay].self, from: data)
        }
        return [:]
    }
    
    static func updateGameMetadata(gameId: String, lastLaunchDate: Date? = nil, sizeMB: UInt64? = nil) {
        var gameDefaults = gameMatadata
        if lastLaunchDate == nil && sizeMB == nil && gameDefaults.keys.contains(gameId) {
            gameDefaults.removeValue(forKey: gameId)
            defaults.set(try? PropertyListEncoder().encode(gameDefaults), forKey: "gmr_game_metadata")
            return
        }
        if !gameDefaults.keys.contains(gameId) {
            gameDefaults[gameId] = GamePlay(gameId: gameId, lastLaunchDate: lastLaunchDate, sizeMB: sizeMB)
            defaults.set(try? PropertyListEncoder().encode(gameDefaults), forKey: "gmr_game_metadata")
            return
        }
        gameDefaults[gameId]?.gameId = gameId
        if lastLaunchDate != nil {
            gameDefaults[gameId]?.lastLaunchDate = lastLaunchDate!
        }
        if sizeMB != nil {
            gameDefaults[gameId]?.sizeMB = sizeMB!
        }
        defaults.set(try? PropertyListEncoder().encode(gameDefaults), forKey: "gmr_game_metadata")
    }
    
    static func clearGameMetadata() {
        defaults.set(try? PropertyListEncoder().encode([:] as [String : GamePlay]), forKey: "gmr_game_metadata")
    }
    
    public struct Game: Codable {
        var gameId : String
        var tag : String
        var archive : String
        var title : String
        var launchPath : String
    }

    public struct GamePlay: Codable {
        var gameId : String
        var lastLaunchDate : Date?
        var sizeMB : UInt64?
    }
//    public static var gameConfig: [Game] = []
    public static var gameConfig = readConfigFiles()

    
    public struct GameLaunch {
        var gameId: String
        var odrTag: String
        var archive: String
        var currency: String
        var locale: String
        var environment: String
        var account : String
        var token : String
        var gameURL : URL
        var gameLaunchPathURL : URL
        var gameArchiveURL : URL?
    }

    private static func readConfigFiles() -> [Game] {
        var gameConfig: [Game] = []
        do {
            if let bundlePath = Bundle.main.path(forResource: "gmr-game-config", ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                do {
                    gameConfig = try JSONDecoder().decode([Game].self, from: jsonData)
                    gameLookup = [:]
                    for (i, game) in gameConfig.enumerated() {
                        gameLookup[game.gameId] = i
                    }
                } catch {
                    os_log("%@", type: .debug, "JSON decode error")
                    os_log("%@", type: .debug, error.localizedDescription)
                }
            }
//            if let bundlePath = Bundle.main.path(forResource: "gmr-game-config-staging", ofType: "json"),
//                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
//                do {
//                    let gameConfig = try JSONDecoder().decode([Game].self, from: jsonData)
//                    gameLookups[1] = [:]
//                    for (i, game) in gameConfig.enumerated() {
//                        gameLookups[1]![game.gameId] = i
//                    }
//                    gameConfigs.append(gameConfig)
//                } catch {
//                    os_log("%@", type: .debug, "JSON decode error")
//                    os_log("%@", type: .debug, error.localizedDescription)
//                }
//            }
//            if let bundlePath = Bundle.main.path(forResource: "v3-game-config", ofType: "json"),
//                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
//                do {
//                    let gameConfig = try JSONDecoder().decode([Game].self, from: jsonData)
//                    gameLookups[0] = [:]
//                    for (i, game) in gameConfig.enumerated() {
//                        gameLookups[0]![game.gameId] = i
//                    }
//                    gameConfigs.append(gameConfig)
//                } catch {
//                    os_log("%@", type: .debug, "JSON decode error")
//                    os_log("%@", type: .debug, error.localizedDescription)
//                }
//            }
        } catch {
            os_log("%@", type: .debug, error.localizedDescription)
        }
        return gameConfig
    }

}
