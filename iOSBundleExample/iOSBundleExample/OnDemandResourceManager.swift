//
//  OnDemandResourceManager.swift
//  iOSBundleExample
//
//  Created by Peter Russell on 11/03/2022.
//

import Foundation
import OSLog

class OnDemandResourceManager {
    
    // MARK: - Properties
    static let shared = OnDemandResourceManager()
    var currentRequest: NSBundleResourceRequest?
    private var observation: NSKeyValueObservation?
    deinit {
        observation?.invalidate()
    }
    
    func requestResourceWith(tag: String,
                             onSuccess: @escaping () -> Void,
                             onFailure: @escaping (NSError) -> Void,
                             onProgress: @escaping (_ percent: Float) -> Void?) {
        
        currentRequest = NSBundleResourceRequest(tags: [tag])
        guard let request = currentRequest else { return }
        request.loadingPriority = NSBundleResourceRequestLoadingPriorityUrgent
        
        request.conditionallyBeginAccessingResources(completionHandler: { (resourceAvailable) in
            if resourceAvailable {
                os_log("%@", type: .debug, "ODR resource already available in Bundle")
                onSuccess()
            } else {
                os_log("%@", type: .debug, "Requesting ODR archive from Apple servers")
                self.observation = request.progress.observe(\.fractionCompleted) { progress, _ in
                    onProgress((Float(progress.fractionCompleted)))
                }
                request.beginAccessingResources { (error: Error?) in
                    if let error = error {
                        onFailure(error as NSError)
                        return
                    }
                    onSuccess()
                }
            }
        })
    }
    
    func dir(pathURL: URL, recursive: Bool, _ indent: Int = 0) {
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: pathURL, includingPropertiesForKeys: nil)
            if directoryContents.isEmpty { return }
            let indentString = String(repeating: " ", count: indent)
            for listEntry in directoryContents {
                os_log("%@", type: .debug, "\(indentString)\(listEntry.lastPathComponent)")
                if recursive {
                    dir(pathURL: listEntry, recursive: true, indent+1)
                }
            }
        } catch {
            //            os_log("%@", type: .debug, "Something went wrong listing \(directory)")
        }
    }
    func done() {
        os_log("%@", type: .debug, "Releasing ODR Tag: \(currentRequest?.tags.description ?? "NONE")")
        currentRequest?.endAccessingResources()
    }
    
    func cleanCache() {
        DispatchQueue.global(qos: .background).async {
            var gameCacheSizeMB = UInt64(0)
            do {
                gameCacheSizeMB = try FileManager.default.allocatedSizeOfDirectory(at: GMRBundleConfiguration.documentRootURL)/1024/1024
                os_log("%@", type: .debug, "Game cache size is \(gameCacheSizeMB)MB, maximum size is \(GMRBundleConfiguration.maxGameCacheSizeMB)MB")
            } catch {
                os_log("%@", type: .debug, "Something went wrong during file sizing \(error.localizedDescription)")
                return
            }
            if gameCacheSizeMB > GMRBundleConfiguration.maxGameCacheSizeMB {
                var spaceToFree = gameCacheSizeMB - GMRBundleConfiguration.maxGameCacheSizeMB
                let games = GMRBundleConfiguration.gameMatadata
                let sortedGames = games.values.sorted(by: {$0.lastLaunchDate ?? Date() < $1.lastLaunchDate ?? Date() } )
                for game in sortedGames {
                    os_log("%@", type: .debug, "Removing \(game.gameId) from cache : \(String(describing: game.sizeMB))MB to achieve \(spaceToFree)MB total reduction")
                    self.removeGameFromFilesystem(gameId: game.gameId)
                    if game.sizeMB ?? UInt64(0) >= spaceToFree {
                        return
                    }
                    spaceToFree = spaceToFree &- (game.sizeMB ?? UInt64(0))
                }
            }
        }
    }
    func resetCache() {
        var directoryContents: [URL] = []
        do {
            directoryContents = try FileManager.default.contentsOfDirectory(at: GMRBundleConfiguration.documentRootURL, includingPropertiesForKeys: nil)
        } catch {
            os_log("%@", type: .debug, "Could not get files at \(GMRBundleConfiguration.documentRootURL)")
            return
        }
        if directoryContents.isEmpty { return }
        directoryContents.forEach {
            print("Removing \($0)")
            do {
                try FileManager.default.removeItem(at: $0)
            } catch {
                os_log("%@", type: .debug, "Could not remove \($0)")
            }
        }
        GMRBundleConfiguration.clearGameMetadata()
    }
    func removeGameFromFilesystem(gameId: String) {
        do {
            try FileManager.default.removeItem(at: GMRBundleConfiguration.documentRootURL.appendingPathComponent(gameId))
            GMRBundleConfiguration.updateGameMetadata(gameId: gameId, lastLaunchDate: nil, sizeMB: nil)
        } catch {
            os_log("%@", type: .debug, "Could not remove \(gameId)")
        }
    }

}

public extension FileManager {
    func allocatedSizeOfDirectory(at directoryURL: URL) throws -> UInt64 {

        var enumeratorError: Error? = nil
        func errorHandler(_: URL, error: Error) -> Bool {
            enumeratorError = error
            return false
        }

        let enumerator = self.enumerator(at: directoryURL,
                                         includingPropertiesForKeys: Array(allocatedSizeResourceKeys),
                                         options: [],
                                         errorHandler: errorHandler)!
        var accumulatedSize: UInt64 = 0

        for item in enumerator {
            if enumeratorError != nil { break }
            let contentItemURL = item as! URL
            accumulatedSize += try contentItemURL.regularFileAllocatedSize()
        }
        if let error = enumeratorError { throw error }
        return accumulatedSize
    }
}


fileprivate let allocatedSizeResourceKeys: Set<URLResourceKey> = [
    .isRegularFileKey,
    .fileAllocatedSizeKey,
    .totalFileAllocatedSizeKey,
]


fileprivate extension URL {

    func regularFileAllocatedSize() throws -> UInt64 {
        let resourceValues = try self.resourceValues(forKeys: allocatedSizeResourceKeys)

        // We only look at regular files.
        guard resourceValues.isRegularFile ?? false else {
            return 0
        }

        // To get the file's size we first try the most comprehensive value in terms of what
        // the file may use on disk. This includes metadata, compression (on file system
        // level) and block size.
        // In case totalFileAllocatedSize is unavailable we use the fallback value (excluding
        // meta data and compression) This value should always be available.
        return UInt64(resourceValues.totalFileAllocatedSize ?? resourceValues.fileAllocatedSize ?? 0)
    }
}
