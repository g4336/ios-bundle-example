//
//  LauncherViewController.swift
//  iOSBundleExample
//
//  Created by Pete Russell on 02/11/2021.
//

import UIKit
import InAppSettingsKit

class LauncherViewController: UIViewController {

    @IBOutlet weak var gameCollection: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var logo: UIImageView!
    
    private let reuseIdentifier = "GameCell"
    private let sectionInsets = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
    private let itemsPerRow: CGFloat = 3
    private var gameList = GMRBundleConfiguration.gameConfig
    private var lastContentOffset : CGFloat = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        if self.traitCollection.userInterfaceStyle == .dark {
            if GMRBundleConfiguration.targetName == "StagingiOSBundleExample" {
                logo.image = UIImage(named: "gmr-logo-horizontal-dark-staging")
            } else {
                logo.image = UIImage(named: "gmr-logo-horizontal-dark")
            }
        } else {
             if GMRBundleConfiguration.targetName == "StagingiOSBundleExample" {
                 logo.image = UIImage(named: "gmr-logo-horizontal-light-staging")
             } else {
                 logo.image = UIImage(named: "gmr-logo-horizontal-light")
             }
        }
        self.gameCollection.dataSource = self
        self.gameCollection.delegate = self
        self.searchBar.delegate = self
    }
    
    @IBAction func didTouchLogo(_ sender: Any) {
        let appSettingsViewController = IASKAppSettingsViewController()
        appSettingsViewController.showDoneButton = false
        appSettingsViewController.showCreditsFooter = false
        appSettingsViewController.delegate = self
        self.navigationController!.pushViewController(appSettingsViewController, animated: true)
    }
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if self.traitCollection.userInterfaceStyle == .dark {
            if GMRBundleConfiguration.targetName == "StagingiOSBundleExample" {
                logo.image = UIImage(named: "gmr-logo-horizontal-dark-staging")
            } else {
                logo.image = UIImage(named: "gmr-logo-horizontal-dark")
            }
        } else {
             if GMRBundleConfiguration.targetName == "StagingiOSBundleExample" {
                 logo.image = UIImage(named: "gmr-logo-horizontal-light-staging")
             } else {
                 logo.image = UIImage(named: "gmr-logo-horizontal-light")
             }
        }
    }

    @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
        print(" Received \(sender.titleForSegment(at: sender.selectedSegmentIndex) ?? "NK")")
        gameList = GMRBundleConfiguration.gameConfig
        GMRBundleConfiguration.gameLookup = GMRBundleConfiguration.gameLookup
        gameCollection.reloadData()
        OnDemandResourceManager.shared.resetCache()
    }

    private func launchGame(gameId: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gameVC = storyboard.instantiateViewController(identifier: "GameViewController") as GameViewController
        let gameIndex = GMRBundleConfiguration.gameLookup[gameId]!
        
        gameVC.gameLaunch = GMRBundleConfiguration.GameLaunch(
            gameId: gameId,
            odrTag: GMRBundleConfiguration.gameConfig[gameIndex].tag,
            archive: GMRBundleConfiguration.gameConfig[gameIndex].archive,
            currency: GMRBundleConfiguration.currency,
            locale: GMRBundleConfiguration.locale,
            environment: GMRBundleConfiguration.environment,
                account: GMRBundleConfiguration.preferenceAccountId,
            token: "\(GMRBundleConfiguration.preferenceAccountId)|currency:"+GMRBundleConfiguration.currency,
            gameURL: GMRBundleConfiguration.launchURL.appendingPathComponent(gameId).appendingPathComponent(GMRBundleConfiguration.gameConfig[gameIndex].launchPath),
            gameLaunchPathURL: GMRBundleConfiguration.documentRootURL.appendingPathComponent(gameId))
        gameVC.modalPresentationStyle = .overFullScreen

        show(gameVC, sender: self)
    }
}

extension LauncherViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return gameList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let gameCell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! GameCell
        gameCell.layer.cornerRadius = 12
        let gameId = gameList[indexPath.item].gameId
        gameCell.gameId = gameId
        if let gameIcon = UIImage(named: gameId) {
            gameCell.gameIcon.image = gameIcon
        } else {
            gameCell.backgroundColor = .lightGray
            gameCell.gameTitle.text = gameList[indexPath.item].title.replacingOccurrences(of: " ", with: "\n")
        }
        return gameCell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let game = collectionView.cellForItem(at: indexPath) as! GameCell
        print("Launching \(game.gameId ?? "NONE")")
        launchGame(gameId: game.gameId!)
    }
}

extension LauncherViewController: UICollectionViewDelegateFlowLayout {

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
//    let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
//    let availableWidth = view.frame.width - paddingSpace
//    let widthPerItem = availableWidth / itemsPerRow
//
//      return CGSize(width: widthPerItem, height: widthPerItem)
      return CGSize(width: 100, height: 100)
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    insetForSectionAt section: Int
  ) -> UIEdgeInsets {
    return sectionInsets
  }

  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    return sectionInsets.left
  }

    
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print("scroll X: \(scrollView.contentOffset.x)")
//        if (self.lastContentOffset > scrollView.contentOffset.x) {
//            print("hide")
//        } else if (self.lastContentOffset < scrollView.contentOffset.x) {
//            print("show")
//        }
//
//        self.lastContentOffset = scrollView.contentOffset.x;
//    }
}

extension LauncherViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        gameList = []
        if searchText == "" {
            gameList = GMRBundleConfiguration.gameConfig
        } else {
            for game in GMRBundleConfiguration.gameConfig {
                if game.title.lowercased().contains(searchText.lowercased()) {
                    gameList.append(game)
                }
            }
        }
        self.gameCollection.reloadData()
    }
}

extension LauncherViewController: IASKSettingsDelegate {
    func settingsViewControllerDidEnd(_ settingsViewController: IASKAppSettingsViewController) {
        //done
    }
    func settingsViewController(_ settingsViewController: IASKAppSettingsViewController, buttonTappedFor specifier: IASKSpecifier) {
        OnDemandResourceManager.shared.resetCache()
    }
}

class GameCell: UICollectionViewCell {
    @IBOutlet weak var gameTitle: UILabel!
    @IBOutlet weak var gameIcon: UIImageView!
    var gameId: String?
    
    override func prepareForReuse() {
        self.gameIcon.image = nil
        self.gameTitle.text = ""
        super.prepareForReuse()
    }
}
