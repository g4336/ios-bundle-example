//
//  GameViewController.swift
//  iOSBundleExample
//
//  Created by Pete Russell on 02/11/2021.
//

import UIKit
import WebKit
import Zip
import OSLog
import AVFoundation

class GameViewController: UIViewController {

    @IBOutlet weak var gameContentView: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var loadingAnimation: UIView!
    var webView : WKWebView!
    var wrapperState : String = ""
    var gameLaunchRequest: URLRequest!
    var gameLaunch: GMRBundleConfiguration.GameLaunch!
    var gameCacheSizeMB: UInt64 = 0
    var canonicalGameFilePath : String {
        if GMRBundleConfiguration.useWebServer {
            return "\(GMRBundleConfiguration.documentRootURL.path)\(gameLaunch.gameURL.path)"
        }
        return gameLaunch.gameURL.path
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.progressBar.isHidden = true
        let contentController = WKUserContentController()
        contentController.add(self, name: "message")
        let script =    """
                        var globalWebKitState = "";
                        window.addEventListener('load', function () {
                            console.log("SENDING to WebKit : LOADED");
                            window.webkit.messageHandlers.message.postMessage("LOADED");
                            function checkWebKitState() {
                                var localWebKitState = "";
                                try { localWebKitState = so.Wrapper.getInstance().model.gameState.getId(); } catch (err) { }
                                if (localWebKitState != "" && localWebKitState != globalWebKitState) {
                                    globalWebKitState = localWebKitState;
                                    console.log("SENDING to WebKit : "+globalWebKitState);
                                    window.webkit.messageHandlers.message.postMessage(globalWebKitState);
                                }
                            }
                            setInterval(checkWebKitState, 1000);
                        });
                        """
        let userScript = WKUserScript(source: script, injectionTime: .atDocumentStart, forMainFrameOnly: true)

        contentController.addUserScript(userScript)

        let webkitConfig = WKWebViewConfiguration()
        webkitConfig.allowsInlineMediaPlayback = true;
        webkitConfig.mediaTypesRequiringUserActionForPlayback = []
        webkitConfig.preferences.setValue(true, forKey: "developerExtrasEnabled")
        webkitConfig.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        webkitConfig.setValue(true, forKey: "allowUniversalAccessFromFileURLs")
        webkitConfig.userContentController = contentController
        if #available(iOS 13.0, *) , GMRBundleConfiguration.forceMobileBrowser {
            /**
             In iOS 13 and above WKWebViews in iPad has the ability to render desktop versions of web pages.
             One of the properties they change to support this is the User-Agent.
             Therefore forcing the WKWebView to load the mobile version.
             */
            let pref = WKWebpagePreferences.init()
            pref.preferredContentMode = .mobile
            webkitConfig.defaultWebpagePreferences = pref
        }

        self.webView = WKWebView(frame: self.gameContentView.bounds, configuration: webkitConfig)
        self.webView.configuration.preferences.setValue(true, forKey: "allowFileAccessFromFileURLs")
        self.webView.configuration.setValue(true, forKey: "allowUniversalAccessFromFileURLs")
        self.webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        if #available(iOS 16.4, *) {
            self.webView.isInspectable = true
        }
        self.title = gameLaunch.archive.replacingOccurrences(of: ".zip", with: "").replacingOccurrences(of: "GMR-", with: "").replacingOccurrences(of: "-ios-", with: " v")
        let gameExistsInDocumentRoot = FileManager.default.fileExists(atPath: gameLaunch.gameURL.path)
        os_log("%@", type: .debug, "GAME PATH: \(gameLaunch.gameURL.path) EXISTS:\(gameExistsInDocumentRoot)")
        
        if gameExistsInDocumentRoot {
            loadGame()
        } else {
            getGameArchive()
        }
    }
    
    func getGameArchive() {
        self.loadingAnimation.isHidden = false
        if let path = Bundle.main.path(forResource: "loader-gmr", ofType:"mp4") {
            let player = AVPlayer(url: URL(fileURLWithPath: path))
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.loadingAnimation.bounds
            self.loadingAnimation.layer.addSublayer(playerLayer)
            player.play()
        } else {
            os_log("%@", type: .debug, "loader-gmr.mp4 not found")
            return
        }
        OnDemandResourceManager.shared.requestResourceWith(tag: gameLaunch.odrTag,
            onSuccess: { [self] in
                DispatchQueue.main.async {
                    self.progressBar.isHidden = true
                    self.loadingAnimation.isHidden = true
                }
                self.gameLaunch.gameArchiveURL = Bundle.main.url(forResource: self.gameLaunch.archive, withExtension: "")

                if self.gameLaunch.gameArchiveURL == nil {
                    os_log("%@", type: .debug, "Could not locate \(self.gameLaunch.archive)")
                    OnDemandResourceManager.shared.dir(pathURL: Bundle.main.bundleURL, recursive: true)
                    self.alertError(message: "There was a problem.\nUnable to locate downloaded ODR resource")
                    return
                }
                let gameArchiveExists = FileManager.default.fileExists(atPath: self.gameLaunch.gameArchiveURL!.path)
                os_log("%@", type: .debug, "ZIP PATH: \(self.gameLaunch.gameArchiveURL?.path ?? "NULL") EXISTS:\(gameArchiveExists)")

                if gameArchiveExists {
                    self.unpackArchive()
                    OnDemandResourceManager.shared.done()
                } else {
                    OnDemandResourceManager.shared.dir(pathURL: Bundle.main.bundleURL, recursive: true)
                    self.alertError(message: "There was a problem.\nZip archive could not be found.")
                    return
                }
                self.loadGame()
            }, onFailure: { (error) in
                DispatchQueue.main.async {
                    self.progressBar.isHidden = true
                    self.loadingAnimation.isHidden = true
                }
                self.alertError(message: "There was a problem retrieving the ODR Archive.\n\(error.localizedDescription)")
            }, onProgress: { (percent) in
                DispatchQueue.main.async {
                    self.progressBar.isHidden = false
                    self.progressBar.progress = percent
                }
            })
    }
    
    func unpackArchive() {
        os_log("%@", type: .debug, "Unzipping: \(gameLaunch.gameArchiveURL?.path ?? "EMPTY") to \(gameLaunch.gameLaunchPathURL)")
        do {
            try Zip.unzipFile(gameLaunch.gameArchiveURL!, destination: gameLaunch.gameLaunchPathURL, overwrite: true, password: nil, progress: nil)

            let unpackedgameExists = FileManager.default.fileExists(atPath: canonicalGameFilePath)
            if !unpackedgameExists {
                os_log("%@", type: .debug, "UNZIP directoryContents: \(OnDemandResourceManager.shared.dir(pathURL: gameLaunch.gameLaunchPathURL, recursive: false))")
                OnDemandResourceManager.shared.removeGameFromFilesystem(gameId: gameLaunch.gameId)
                self.alertError(message: "There was a problem.\nSomething went wrong during unzip - expected launch file does not exist:\n\(canonicalGameFilePath) ")
                return
            }
            
            DispatchQueue.global(qos: .background).async {
                do {
                    let gameSizeMB = try FileManager.default.allocatedSizeOfDirectory(at: self.gameLaunch.gameLaunchPathURL)/1024/1024
                    GMRBundleConfiguration.updateGameMetadata(gameId: self.gameLaunch.gameId, sizeMB: gameSizeMB)
                } catch {
                    os_log("%@", type: .debug, "Unable to size \(self.gameLaunch.gameLaunchPathURL)")
                    GMRBundleConfiguration.updateGameMetadata(gameId: self.gameLaunch.gameId, sizeMB: nil)
                }
                var values = URLResourceValues()
                values.isExcludedFromBackup = true
                try? self.gameLaunch.gameLaunchPathURL.setResourceValues(values)
            }
        } catch {
            self.alertError(message: "There was a problem.\nSomething went wrong during unzip\n\(error.localizedDescription)")
            OnDemandResourceManager.shared.removeGameFromFilesystem(gameId: gameLaunch.gameId)
            os_log("%@", type: .debug, "Something went wrong during unzip")
        }
    }
    
    func loadGame() {
        OnDemandResourceManager.shared.cleanCache()
        let gameExists = FileManager.default.fileExists(atPath: canonicalGameFilePath)
        os_log("%@", type: .debug, "GAME PATH: \(canonicalGameFilePath) EXISTS:\(gameExists)")
        
        if !gameExists {
            self.alertError(message: "There was a problem.\nGame could not be found\n\(canonicalGameFilePath)")
            os_log("%@", type: .debug, "Game could not be found in DocumentRoot")
            OnDemandResourceManager.shared.removeGameFromFilesystem(gameId: gameLaunch.gameId)
            return
        }
        let gameLaunchQuery = [
            URLQueryItem(name: "gameServer", value: gameLaunch.environment),
            URLQueryItem(name: "gameId", value: gameLaunch.gameId),
            URLQueryItem(name: "accountId", value: gameLaunch.account),
            URLQueryItem(name: "token", value: gameLaunch.token),
            URLQueryItem(name: "operator", value: "gr-test"),
            URLQueryItem(name: "currency", value: gameLaunch.currency),
            URLQueryItem(name: "locale", value: gameLaunch.locale)
        ]

        var gameURLComponents = URLComponents(url: gameLaunch.gameURL, resolvingAgainstBaseURL: false)
        gameURLComponents?.queryItems = gameLaunchQuery
        os_log("%@", type: .debug, "LOADING: \(gameURLComponents?.url?.absoluteString ?? "EMPTY")")
        if gameURLComponents?.url == nil {
            self.alertError(message: "There was a problem.\nLaunch URL invalid: \(gameURLComponents?.url?.absoluteString ?? "EMPTY")")
            return
        }
        gameLaunchRequest = URLRequest(url: gameURLComponents!.url!)
        GMRBundleConfiguration.updateGameMetadata(gameId: self.gameLaunch.gameId, lastLaunchDate: Date())

        DispatchQueue.main.async {
            os_log("%@", type: .debug, "LOADING Game GCD-ASYNC")
            self.webView.load(self.gameLaunchRequest)
            self.gameContentView.addSubview(self.webView!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        webView.frame = self.gameContentView.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        webView.stopLoading()
        webView.navigationDelegate = nil
        webView.scrollView.delegate = nil
        webView.configuration.userContentController.removeAllContentRuleLists()
        webView = nil
    }
    
    func alertError(message: String) {
        
        DispatchQueue.main.async {
            let controller = UIAlertController(
              title: "Error",
              message: message,
              preferredStyle: .alert)

            controller.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
            guard let rootViewController = self.view?.window?.rootViewController else { return }
            rootViewController.present(controller, animated: true)
        }
    }
}

extension GameViewController: WKNavigationDelegate {
    
    // 1. Decides whether to allow or cancel a navigation.
    public func webView(_ webView: WKWebView,
                        decidePolicyFor navigationAction: WKNavigationAction,
                        decisionHandler: @escaping (WKNavigationActionPolicy) -> Swift.Void) {
        
        os_log("%@", type: .debug, "navigationAction load:\(String(describing: navigationAction.request.url))")
        decisionHandler(.allow)
        
    }
    
    // 2. Start loading web address
    func webView(_ webView: WKWebView,
                 didStartProvisionalNavigation navigation: WKNavigation!) {
        os_log("%@", type: .debug, "start load:\(String(describing: webView.url))")
    }
    
    // 3. Fail while loading with an error
    func webView(_ webView: WKWebView,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
        os_log("%@", type: .debug, "fail with error: \(error.localizedDescription)")
    }
    
    // 4. WKWebView finish loading
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        os_log("%@", type: .debug, "finish loading")
    }
}

extension GameViewController: WKScriptMessageHandler {
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("Received message: \(message.name) BODY: \(message.body)")
        self.wrapperState = message.body as! String;
    }
}
