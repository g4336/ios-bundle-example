//
//  iOSBundleExampleTests.swift
//  iOSBundleExampleTests
//
//  Created by Pete Russell on 02/11/2021.
//

import XCTest
import WebKit
@testable import iOSBundleExample

class iOSBundleExampleTests: XCTestCase, WKNavigationDelegate {

    var gameController: GameViewController!
    var exp: XCTestExpectation!
    
    override func setUpWithError() throws {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        gameController = storyboard.instantiateViewController(identifier: "GameViewController") as GameViewController
        
        let gameId = "slingo-tetris"
        let gameIndex = GMRBundleConfiguration.gameLookup[gameId]!
        
        gameController.gameLaunch = GMRBundleConfiguration.GameLaunch(
            gameId: gameId,
            odrTag: GMRBundleConfiguration.gameConfig[gameIndex].tag,
            archive: GMRBundleConfiguration.gameConfig[gameIndex].archive,
            currency: GMRBundleConfiguration.currency,
            locale: GMRBundleConfiguration.locale,
            environment: GMRBundleConfiguration.environment,
            account: GMRBundleConfiguration.preferenceAccountId,
            token: "\(GMRBundleConfiguration.preferenceAccountId)|currency:"+GMRBundleConfiguration.currency,
            gameURL: GMRBundleConfiguration.launchURL.appendingPathComponent(gameId).appendingPathComponent(GMRBundleConfiguration.gameConfig[gameIndex].launchPath),
            gameLaunchPathURL: GMRBundleConfiguration.documentRootURL.appendingPathComponent(gameId))

        gameController.modalPresentationStyle = .overFullScreen
        gameController.loadViewIfNeeded()
    }

    override func tearDownWithError() throws {
        gameController = nil
        super.tearDown()
    }

//    func testWebViewContentLoad() throws {
//        
//        gameController.loadGame()
//        let expectation = expectation(description: "GameDidLoad")
//        DispatchQueue.main.asyncAfter(deadline: .now() + 15, execute: {
//            print("Game Load Wrapper state: \(self.gameController.wrapperState)")
//            XCTAssertEqual(self.gameController.wrapperState, "GameState.PRELOAD_COMPLETE")
//            expectation.fulfill()
//        })
//        wait(for: [expectation], timeout: 16.0)
//    }

    
//    func testServerStart() {
//        self.exp = expectation(description: "WebServerStart")
//    }
    func testWebViewLoading() {
        self.exp = expectation(description: "TetrisLoad")
        

        let webView = WKWebView()
        webView.navigationDelegate = self
        let url = URL(string: "https://alchemy-sta.gamingrealms.net/rgs/games/slingo/tetris/rgs.html?gameServer=https%3A%2F%2Frgs-eu-sta.gamingrealms.net&gameId=slingo-tetris&accountId=pete&locale=en-GB&currency=GBP&operator=gr-test&token=1708684019770%7Ccurrency%3AGBP%7C&device=tablet&forceDevice=tablet&pcurl=https%3A%2F%2Falchemy-sta.gamingrealms.net%2Frgs%2Fwrapper%2Fso-wrapper.min.js%3F1709566739705&roomName=Slingo%20Tetris%20-%20Desktop")!
        webView.load(URLRequest(url: url))

        waitForExpectations(timeout: 10, handler: nil)
    }

    // WKNavigationDelegate method
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        // Notify the expectation
        self.exp.fulfill()
    }
}
