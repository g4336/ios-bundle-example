//
//  GCDWebServer.h
//  GCDWebServer
//
//  Created by Pete Russell on 02/11/2021.
//

#import <Foundation/Foundation.h>

//! Project version number for GCDWebServer.
FOUNDATION_EXPORT double GCDWebServerVersionNumber;

//! Project version string for GCDWebServer.
FOUNDATION_EXPORT const unsigned char GCDWebServerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GCDWebServer/PublicHeader.h>


