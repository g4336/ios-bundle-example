//
//  iOSBundleExampleUITestsLaunchTests.swift
//  iOSBundleExampleUITests
//
//  Created by Pete Russell on 02/11/2021.
//

import XCTest

class iOSBundleExampleUITestsLaunchTests: XCTestCase {
    

    override class var runsForEachTargetApplicationUIConfiguration: Bool {
        false
    }

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    func testLaunch() throws {
        let app = XCUIApplication()
        app.launch()

        // Insert steps here to perform after app launch but before taking a screenshot,
        // such as logging into a test account or navigating somewhere in the app

        let attachment = XCTAttachment(screenshot: app.screenshot())
        attachment.name = "Launch Screen"
        attachment.lifetime = .keepAlways
        add(attachment)
    }
    
    func testGameLoad() throws {
        let app = XCUIApplication()
        app.launch()
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 0).otherElements.containing(.staticText, identifier:"Label").element.tap()
        let backButton = app.navigationBars["iOSBundleExample.GameView"].buttons["Back"]
        backButton.tap()
    }
}
