# Scripts and Tools

> [!WARNING]
> These scripts may corrupt your project.pbxproj file and render your project unusable.
> PLease take care and take many, many backups of you files.
> No waranty is provided for these scripts.

Having to build and rebuild the app many times with updated versions of the archive bundle files becomes tedious and error-prone.
As a result, we have built scripts that automates some of these tasks.

## tag.js

Tagging bundles is a particularly onerous piece of work, particularly when there are significant numbers of game files to be deployed.
The [tag.js](../tools/tag.js) is a node script that reads through the project's project.pbxproj and applies a tag to each bundle file, using a naming convention based on the filename.

The script simply reads through your project.pbxproj file and does re regular-expression search and replace to apply the generated tag name to the file and add a list of all the tags to the tag array.
For GMR files, the search regex is:
```/(^.*isa = PBXBuildFile; fileRef = .* (GMR-.*?)*-ios.*\.zip \*\/;).*$/```
and the replace REGEX is: ```/KnownAssetTags = \(([\s\S]*?)\);/```
You may need to change this if your file naming convention differs.

You *will* need to change the path to your project.pbxproj file.
```var projectFile = '../iOSBundleExample/iOSBundleExample.xcodeproj/project.pbxproj';```

You run the script as follows:
```shell
cd tools
node tag.js
```
and the script writes the new file to output.pbxproj in the tools directory.
This should be used as a replacement for your project's project.pbxproj file (please back up your existing file first). When you replace the file XCode should refresh and you will see teh new tags against each file.

### For The Brave (or foolhardy)
Additionally the ```run.sh``` will run the run.js script and copy the output into the correct place, backing up your current file into the /tmp/ directory.
I would be very careful before using this and make sure your amended scripts are running flawlessly before you use this.