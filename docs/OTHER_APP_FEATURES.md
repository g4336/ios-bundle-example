# Other App Features
The example app also has some other features and nice-to-haves that are included by way of example and potentially help with building a fully featured Operator app.

## Mark ODR Assets for Cleanup
As soon as a game archive has been retreived from teh ODR Servers and unpacked to the "Docuemnt Root" then the zip archive becmes redundant, it is good practice to then teel the OS that this can then be cleaned up and deleted if space is needed.
```swift
//  OnDemandResourceManager.swift
//  iOSBundleExample
    func done() {
        os_log("%@", type: .debug, "Releasing ODR Tag: \(currentRequest?.tags.description ?? "NONE")")
        currentRequest?.endAccessingResources()
    }
```

## Do Not Backup Game Assets to iCloud
Many users have limited iCloud storage and they won't thank app developers for filling it with unnecesarry files. You can mark directories as being unavailable for backup so as to minimise the impact user's on iCloud storage quotas.
```swift
//  GameViewController.swift
//  iOSBundleExample

	var values = URLResourceValues()
	values.isExcludedFromBackup = true
	try? self.gameLaunch.gameLaunchPathURL.setResourceValues(values)
```

## Game Cache Maintenance
As game archives are downloaded from Apples ODR Servers and unpacked into the app's "Document Root" directory, the size of the app will grow.
The iOS management systems will monitor and remove items as space on the device fills up, but it is better to manage this proactively and even allow the player to set their own limits.

The example app has a rudimenary system for managing the size of the downloaded files.
The user can set a maximum size for the game cache thsi defaults to 200MB and is stored in UserDefaults for persistence.
```swift
//  GMRBundleConfiguration.swift
//  iOSBundleExample

    public static var maxGameCacheSizeMB: UInt64 {
        return UInt64(defaults.string(forKey: "cache_size_preference") ?? "200") ?? 200
    }
```

Game metadata is also stored in the UserDefaults and stores the last launch date of the game and the space it takes up on disk.
```swift
//  GMRBundleConfiguration.swift
//  iOSBundleExample

    public static var gameMatadata: [String : GamePlay] {
        if let data = defaults.object(forKey: "gmr_game_metadata") as? Data {
            return try! PropertyListDecoder().decode([String : GamePlay].self, from: data)
        }
        return [:]
    }
    
    static func updateGameMetadata(gameId: String, lastLaunchDate: Date? = nil, sizeMB: UInt64? = nil) {
        var gameDefaults = gameMatadata
        if lastLaunchDate == nil && sizeMB == nil && gameDefaults.keys.contains(gameId) {
            gameDefaults.removeValue(forKey: gameId)
            defaults.set(try? PropertyListEncoder().encode(gameDefaults), forKey: "gmr_game_metadata")
            return
        }
        if !gameDefaults.keys.contains(gameId) {
            gameDefaults[gameId] = GamePlay(gameId: gameId, lastLaunchDate: lastLaunchDate, sizeMB: sizeMB)
            defaults.set(try? PropertyListEncoder().encode(gameDefaults), forKey: "gmr_game_metadata")
            return
        }
        gameDefaults[gameId]?.gameId = gameId
        if lastLaunchDate != nil {
            gameDefaults[gameId]?.lastLaunchDate = lastLaunchDate!
        }
        if sizeMB != nil {
            gameDefaults[gameId]?.sizeMB = sizeMB!
        }
        defaults.set(try? PropertyListEncoder().encode(gameDefaults), forKey: "gmr_game_metadata")
    }
```

Every time a game is launched the app kicks off a task to manage the cache.
```swift
OnDemandResourceManager.shared.cleanCache()
```
This task runs on a background thread and looks at the total taken up on disk by the downloaded games and compares to the user's stored preference. If it is larger tahn the user's preferred cache size, it runs a cleanup process:
- Sort the game metadata by launch data ascending (most recently launched last)
- Loop through the games deleting the assets for the least recently played
- Check again to see if the game cache is within the user's preferred cache size
- Continue until game cache is within user preference limits.